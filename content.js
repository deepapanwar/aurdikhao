chrome.runtime.onMessage.addListener(function(request, sender, response) {
    if (request.type === 'check') {
        process();
    }
});

function process() {
    var title = $(document).find("title").text(); 

    if(title == "Place Your Order - Amazon.in Checkout" || title == "Amazon.in Shopping Cart") {
        var name = $(".asin-title").html();
        var price = $($(".shipping-group").find(".a-color-price").find(".currencyINR")[0]).parent().contents().text();
        price = price.trim();
        price = price.substring(3).trim();
        chrome.runtime.sendMessage({"name": name, "price": price});
    }
}
