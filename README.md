# README #

##What is Aurdikhao ?##
AurDikhao is a chrome extension for searching similar items user is currently viewing in a shopping site.

##How do I get set up?##
1. git clone https://deepapanwar@bitbucket.org/deepapanwar/aurdikhao.git
1. Open chrome://extensions/ in google chrome 
2. Click on "load unpacked extension" and select downloaded the project folder
3. Enable developer mode
4. Aurdikhao extension is added to chrome
5. To use the extension visit any shopping site and open a product, say [http://www.amazon.in/gp/product/B00QF7JW3S](http://www.amazon.in/gp/product/B00QF7JW3S). Click the extension icon to see similar products in other sites.