package com.revendale.controller;

import com.revendale.model.ProductDetails;
import com.revendale.repository.ProductDetailsRepo;
import com.revendale.service.FlipkartScanner;
import com.revendale.service.SnapdealScanner;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class AurdikhaoController {
    private static Logger logger = org.slf4j.LoggerFactory.getLogger(AurdikhaoController.class);

    @Autowired
    private SnapdealScanner snapdealScanner;
    @Autowired
    private FlipkartScanner flipkartScanner;
    @Autowired
    private ProductDetailsRepo productDetailsRepo;

    @RequestMapping(value = "/scan", method = RequestMethod.GET)
    @ResponseBody
    public Boolean scanFolders(HttpServletRequest request, HttpServletResponse response) {
        //snapdealScanner.scanFolders();
        flipkartScanner.scan();
        return true;
    }

    @RequestMapping(value = "/search", method=RequestMethod.GET)
    @ResponseBody
    public List<ProductDetails> search(HttpServletRequest request, HttpServletResponse response) {
        String query = request.getParameter("query");
        int offset = 0;
        if(request.getParameterMap().containsKey("offset")) {
            offset = Integer.parseInt(request.getParameter("offset"));
        }
        logger.info("Searching for {} offset {}", query, offset);
        List<ProductDetails> result = productDetailsRepo.searchProducts(query, offset);
        logger.info("Found {} results for {}, {}", result.size(), query, offset);
        return result;
    }


}
