package com.revendale;


import com.revendale.util.AppProperties;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.DispatcherServlet;

@Service
public class AppStart {
    private static Logger logger = org.slf4j.LoggerFactory.getLogger(AppStart.class);


    public  static  void  main(String[] args) throws  Exception{
        Server server = new Server(AppProperties.APP_PORT);
        ServletHolder servletHolder = new ServletHolder(DispatcherServlet.class);
        servletHolder.setInitParameter("contextConfigLocation", "classpath:context.xml");
        servletHolder.setInitOrder(1);


        ServletContextHandler servletContextHandler = new ServletContextHandler(server,"/*");
        servletContextHandler.addServlet(servletHolder,"/*");
        //servletContextHandler.setResourceBase(new ClassPathResource("webapp").getURI().toString());
        server.setHandler(servletContextHandler);
        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread(
                () -> logger.info("Trying to shutdown aurdikhao cleanly")
        ));
    }
}
