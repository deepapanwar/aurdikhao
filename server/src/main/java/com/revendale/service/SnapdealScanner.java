package com.revendale.service;

import com.revendale.model.ProductDetails;
import com.revendale.repository.ProductDetailsRepo;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SnapdealScanner {
    private static Logger logger = org.slf4j.LoggerFactory.getLogger(SnapdealScanner.class);
    @Autowired
    private ProductDetailsRepo productDetailsRepo;

    private void loadFile(String site, File f) {
        logger.info("Loading data of {} from {}", site, f.getName());
        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            List<ProductDetails> cache =
                    br.lines()
                    .skip(1)
                    .parallel()
                    .map(line -> line.split("\\t"))
                    .filter(parts -> parts.length == 14)
                    .map(parts -> {
                        ProductDetails productDetails = new ProductDetails(
                                parts[1],
                                parts[4],
                                parts[5],
                                parts[3],
                                site,
                                Long.parseLong(parts[13]) * 100L
                        );
                        return productDetails;
                    })
                    .collect(Collectors.toCollection(ArrayList::new));

            logger.info("Saving {} products", cache.size());
            productDetailsRepo.saveProductBatch(cache);
        } catch (Exception ex) {
            logger.error("Error while scanning {}", f.getName(), ex);
        }
    }

    private void scan(String site) {
        String path = "/home/deepap/projects/aurdikhao/" + site + "_data/";
        logger.info("Scanning {} at {}", site, path);
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        for(File file: listOfFiles) {
            if(file.isFile() && file.getName().endsWith(".csv")) {
                loadFile(site, file);
            }
        }
    }

    public void scanFolders() {
        scan("snapdeal");
    }
}
