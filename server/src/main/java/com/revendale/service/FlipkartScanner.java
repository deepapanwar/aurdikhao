package com.revendale.service;


import com.google.gson.*;
import com.revendale.model.ProductDetails;
import com.revendale.repository.ProductDetailsRepo;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

/**
 * Created by balajiganapathi on 27/2/16.
 */
@Service
public class FlipkartScanner {
    private static Logger logger = org.slf4j.LoggerFactory.getLogger(FlipkartScanner.class);
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ProductDetailsRepo productDetailsRepo;

    public Boolean scan() {
        logger.info("Scanning started");
        Map<String, String> categories = fetchCategories();
        logger.info("Scanning ended");
        categories.entrySet()
                .parallelStream()
                .forEach(entry -> scanCategory(entry.getKey(), entry.getValue()));
        return true;
    }

    public boolean scanCategory(String category, String url) {
        logger.info("Scanning {} url {}", category, url);
        HttpEntity<String> requestEntity = new HttpEntity<>("{}", getHeaders());
        do {
            try {
                ResponseEntity<String> response = restTemplate.exchange(
                        url + "&inStock=true",
                        HttpMethod.GET,
                        requestEntity,
                        String.class,
                        Collections.emptyMap());
                if (response.getStatusCode() == HttpStatus.OK) {
                    JsonObject responseJson = new Gson().fromJson(response.getBody(), JsonObject.class);
                    JsonArray productArray = responseJson.getAsJsonArray("productInfoList");
                    logger.info("Found {} products in category {}", productArray.size(), category);
                    List<ProductDetails> products = processProducts(productArray);
                    logger.info("Saving to db...");
                    productDetailsRepo.saveProductBatch(products);
                    if(responseJson.has("nextUrl") && !responseJson.get("nextUrl").isJsonNull()) {
                        url = responseJson.getAsJsonPrimitive("nextUrl").getAsString();
                        logger.info("Continuing scanning {}", url);
                    } else {
                        break;
                    }
                } else {
                    throw new RuntimeException("Request not ok. Returned " + response.getStatusCode());
                }
            } catch (Exception ex) {
                logger.error("Error while fetching category {}", category, ex);
            }
        } while(true);
        return true;
    }

    List<ProductDetails> processProducts(JsonArray productsJson) {
        List<ProductDetails> products = new LinkedList<>();
        for(JsonElement element: productsJson) {
            products.add(buildProduct(element.getAsJsonObject()));
        }
        return products;
    }

    ProductDetails buildProduct(JsonObject json) {
        JsonObject productJson = getJsonAt(json, "productBaseInfo.productAttributes").getAsJsonObject();
        JsonObject imageURLMap = productJson.getAsJsonObject("imageUrls");
        String imageURL = "";
        if(imageURLMap.entrySet().size() == 0) {
            logger.debug("No imageUrl found: {}", productJson);
        } else {
            imageURL = ((JsonPrimitive) ((Map.Entry) ((imageURLMap.entrySet().toArray())[0])).getValue()).getAsString();
        }
        return new ProductDetails(
                getAsString(productJson.get("title")),
                getAsString(productJson.get("productUrl")),
                imageURL,
                getAsString(productJson.get("productBrand")),
                "flipkart",
                getJsonAt(productJson, "sellingPrice.amount").getAsLong() * 100L);
    }


    Map<String, String> fetchCategories() {
        Map<String, String> categories = new HashMap<>();
        HttpEntity<String> requestEntity = new HttpEntity<>("{}", getHeaders());
        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    "https://affiliate-api.flipkart.net/affiliate/api/dppnwr3gm.json?inStock=true",
                    HttpMethod.GET,
                    requestEntity,
                    String.class,
                    Collections.emptyMap());
            if(response.getStatusCode() == HttpStatus.OK) {
                categories = processCategories(response.getBody());
            } else {
                throw new RuntimeException("Request not ok. Returned " + response.getStatusCode());
            }
        } catch (Exception ex) {
            logger.error("Error while fetching categories on Flipkart: {}", ex);
        }
        logger.info("Fetched {} categories", categories.size());
        return categories;
    }

    Map<String, String> processCategories(String responseJson) {
        Map<String, String> categories = new HashMap<>();
        JsonObject categoriesJson = getJsonAt(new Gson().fromJson(responseJson, JsonObject.class),
                "apiGroups.affiliate.apiListings").getAsJsonObject();
        for(Map.Entry entry: categoriesJson.entrySet()) {
            String key = (String)entry.getKey();
            JsonObject val = getJsonAt((JsonElement)entry.getValue(), "availableVariants").getAsJsonObject();
            Iterator iter = val.entrySet().iterator();
            if(!iter.hasNext()) {
                throw new RuntimeException("Malformed JSON " + val.toString());
            }
            String url = ((JsonElement)((Map.Entry)iter.next()).getValue())
                    .getAsJsonObject()
                    .get("get")
                    .getAsString();
            categories.put(key, url);
        }

        return categories;
    }

    HttpHeaders getHeaders() {
        List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
        acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(acceptableMediaTypes);
        headers.setContentType(MediaType.APPLICATION_JSON);
        // TODO: Put in properties file
        headers.add("Fk-Affiliate-Id", "dppnwr3gm");
        headers.add("Fk-Affiliate-Token", "07614885a3394928a2a2e4ec4f889454");

        return headers;
    }

    JsonElement getJsonAt(JsonElement json, String path) {
        String [] pathArray = path.split("\\.");

        for(int i = 0; i < pathArray.length - 1; i++) {
            json = json.getAsJsonObject().getAsJsonObject(pathArray[i]);
        }
        if(pathArray.length > 0) {
            json = json.getAsJsonObject().get(pathArray[pathArray.length - 1]);
        }

        return json;
    }

    String getAsString(JsonElement element) {
        if(element.isJsonNull()) return "";
        if(element.isJsonPrimitive()) return element.getAsString();
        String str = element.toString();
        if(str.startsWith("\"")) str = str.substring(1);
        if(str.endsWith("\"")) str = str.substring(0, str.length() - 2);
        return str;
    }
}
