package com.revendale.model;


public class ProductDetails {
    private String name;
    private String productURL;
    private String imageURL;
    private String site;
    private String brand;
    private long price; // In paise

    public ProductDetails() {
    }

    public ProductDetails(String name,
                          String productURL,
                          String imageURL,
                          String brand,
                          String site,
                          long price) {

        this.name = name;
        this.productURL = productURL;
        this.imageURL = imageURL;
        this.price = price;
        this.brand = brand;
        this.site = site;
    }

    public String getName() {
        return name;
    }

    public String getProductURL() {
        return productURL;
    }

    public String getImageURL() {
        return imageURL;
    }

    public long getPrice() {
        return price;
    }

    public String getSite() {
        return site;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProductURL(String productURL) {
        this.productURL = productURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public void setPrice(long price) {
        this.price = price;
    }
}
