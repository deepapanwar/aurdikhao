package com.revendale.repository;

import com.revendale.model.ProductDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductDetailsMapper implements RowMapper<ProductDetails> {
    @Override
    public ProductDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
        ProductDetails productDetails = new ProductDetails();
        productDetails.setName(rs.getString("name"));
        productDetails.setImageURL(rs.getString("imageURL"));
        productDetails.setPrice(rs.getLong("price"));
        productDetails.setProductURL(rs.getString("productURL"));
        productDetails.setBrand(rs.getString("brand"));
        productDetails.setSite(rs.getString("site"));
        return productDetails;
    }
}
