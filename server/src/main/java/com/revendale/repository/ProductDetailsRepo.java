package com.revendale.repository;

import com.revendale.model.ProductDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class ProductDetailsRepo {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LoggerFactory.getLogger(ProductDetailsRepo.class);

    public boolean saveProduct(ProductDetails newProductDetails) {
        try {
            jdbcTemplate.update("insert into productdetails(name, site, productURL, imageURL, price) values (?, ?, ?, ?, ?)", newProductDetails.getName(), newProductDetails.getSite(), newProductDetails.getProductURL(), newProductDetails.getImageURL(), newProductDetails.getPrice());
            return true;
        }catch (Exception ex) {
            logger.error("Exception while updating product", ex);
            return false;
        }
    }
    public boolean saveProductBatch(List<ProductDetails> productDetailsList) {
        try {
            jdbcTemplate.batchUpdate("insert into productdetails(name, site, productURL, imageURL, brand, price) values (?, ?, ?, ?, ?, ?)", new BatchPreparedStatementSetter() {
                        @Override
                        public void setValues(PreparedStatement ps, int i) throws SQLException {
                            ProductDetails productDetails = productDetailsList.get(i);
                            ps.setString(1, productDetails.getName());
                            ps.setString(2, productDetails.getSite());
                            ps.setString(3, productDetails.getProductURL());
                            ps.setString(4, productDetails.getImageURL());
                            ps.setString(5, productDetails.getBrand());
                            ps.setLong(6, productDetails.getPrice());
                        }

                        @Override
                        public int getBatchSize() {
                            return productDetailsList.size();
                        }
                    });
            return true;
        }catch (Exception ex) {
            logger.error("Exception while batch updating products", ex);
            return false;
        }
    }

    public List<ProductDetails> searchProducts(String query, int offset) {
        List<Map<String, Object>> response = jdbcTemplate.queryForList(
                "SELECT * from productdetails " +
                "WHERE MATCH name AGAINST (? IN NATURAL LANGUAGE MODE) " +
                "LIMIT ?, 9",
                query,
                offset);

        return response
                .stream()
                .map(row ->
                    new ProductDetails(
                            (String)row.get("name"),
                            (String)row.get("productURL"),
                            (String)row.get("imageURL"),
                            (String)row.get("brand"),
                            (String)row.get("site"),
                            (Long)row.get("price")
                    )
                )
                .collect(Collectors.toList());
    }
}
