package com.revendale.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Created by deepap on 26/2/16.
 */

public class AppProperties {
    public  static final int APP_PORT =  Integer.parseInt(getProperty("jetty.port"));
    public static String getProperty(final String key) {

        final ResourceBundle resourceBundle = ResourceBundle.getBundle("application");
        String propertyValue = null;

        try {
            propertyValue = resourceBundle.getString(key);
        } catch (final MissingResourceException e) {
        }
        return propertyValue;
    }
}
