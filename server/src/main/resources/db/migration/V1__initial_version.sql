CREATE TABLE productdetails (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(1000),
    site VARCHAR(1000),
    productURL VARCHAR(1000),
    imageURL VARCHAR(1000),
    brand VARCHAR(1000),
    price BIGINT);



