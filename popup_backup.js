function setStatus(message) {
    $("#status").html(message);
}

function removeBrackets(title) {
    var ret = "";
    var b = 0;
    console.log(title);
    for(var i = 0; i < title.length; i++) {
        var c = title[i];
        if(b > 0) {
            if(c == ')') {
                b -= 1;
                if(b == 0) ret += " ";
            }
        } else if(c == '(') {
            b += 1;
        } else {
            ret += c;
        }
    }
    console.log(ret);
    return ret;
}

function extractProductName(title) {
    setStatus("Extracting title...");
    var sep = "|:-";
    productTitle = title;
    if(title.indexOf("Amazon.in") != -1) {
        productTitle = title.substring(0, title.indexOf(":"));
    } else if(title.indexOf("Flipkart.com") != -1) {
        productTitle = title.substring(0, title.indexOf("-"));
    } else {
        var till = title.length;
        for(var i = 0; i < sep.length; ++i) {
            var idx = title.indexOf(sep[i]);
            if(idx != -1 && idx < till) till = idx;
        }
        productTitle = productTitle.substring(0, till);
    }
    productTitle = removeBrackets(productTitle);
    productTitle = productTitle.trim();
    console.log(productTitle);

    return productTitle;
}

function populateDisplay(i, p, logoURL) {
    imageURL = p.productBaseInfo.productAttributes.imageUrls["400x400"];
    productURL = p.productBaseInfo.productAttributes.productUrl;
    price = p.productBaseInfo.productAttributes.sellingPrice.amount;
    name = p.productBaseInfo.productAttributes.title;

    $("#image" + i).attr("src", imageURL);
    $("#price" + i).html("&#8377; " + price);
    $("#name" + i).html(name);
    $("#logo" + i).attr("src", logoURL);
    $("#link" + i).attr("href", productURL);
}

function parseFlipkartResult(result) {
    console.log(result);
    products = result.productInfoList;
    if(products.length == 0) {
        setStatus("No products returned by flipkart");
        return;
    }
    setStatus("Processing products returned by flipkart");
    for(var i = 0; i < 9; i = i + 1) {
        populateDisplay(i + 1, products[i % products.length], "flipkart.png");
    }
    setStatus("Enjoy!");
}

function queryFlipkart(title) {
    var queryURL = "https://affiliate-api.flipkart.net/affiliate/search/json?resultCount=9&query=" + encodeURI(title);

    var xhr = new XMLHttpRequest();
    xhr.open("GET", queryURL, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Fk-Affiliate-Id", "dppnwr3gm");
    xhr.setRequestHeader("Fk-Affiliate-Token", "07614885a3394928a2a2e4ec4f889454");
    xhr.onloadend = function() {
        var result = JSON.parse(xhr.responseText);
        parseFlipkartResult(result);
    };
    rawData = {};
    data = JSON.stringify(rawData);

    console.log(data);
    xhr.onerror = function() {
        setStatus("Sorry. Some error occured while querying flipkart");
    }
    xhr.send(data);

}

function queryAmazon(title) {
    console.log("Amazon query...");
    console.log(credentials);
    url = generateQueryString(title, 'ItemSearch', credentials);
    console.log(url);
}

function processPage(tab) {
    title = extractProductName(tab.title);
    setStatus("Searching for " + title);
    queryFlipkart(title);
    queryAmazon(title);
};

function getActiveTab() {
    var queryFilter = {
        active: true,
        currentWindow: true
    };
    chrome.tabs.query(queryFilter, function(tabs) {
        processPage(tabs[0]); 
    });
}


document.addEventListener('DOMContentLoaded', getActiveTab);
