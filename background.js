chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if(changeInfo.status == "complete") {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
            chrome.tabs.sendMessage(tabs[0].id, {type: 'check'});
        });
    }
});

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    var queryURL = "http:localhost:8080/search?query=" + encodeURI(request.name) + "&offset=0";

    var xhr = new XMLHttpRequest();
    xhr.open("GET", queryURL, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onloadend = function() {
        var result = JSON.parse(xhr.responseText);
        process(result, request.name, request.price);
    };
    rawData = {};
    data = JSON.stringify(rawData);

    console.log(data);
    xhr.onerror = function() {
        setStatus("Sorry. Some error occured while querying server");
    }
    xhr.send(data);
});

function process(result, name, price) {
    var priceInPaise = parseFloat(price) * 100;
    var best = priceInPaise;
    for(i = 0; i < result.length; i += 1) {
        var currentPrice = parseFloat(result[i].price);
        if(currentPrice < best) {
            best = currentPrice;
        }
    }
    if(best < priceInPaise) {
        chrome.storage.local.set({'search': name});
        best = best / 100.0;
        alert("WOW! You are buying " + name + " for " + price + ". We found a better deal for as low as " + best + ". Please press the AurDikhao icon to see.");
    } else {
        alert("Go on! Great deal!"); // TODO: Remove this
    }
}
