var history = [];
var searchText = null;
var nextFetchDistance = 200;

function setStatus(message) {
    $("#status").html(message);
}

function removeBrackets(title) {
    var ret = "";
    var b = 0;
    console.log(title);
    for(var i = 0; i < title.length; i++) {
        var c = title[i];
        if(b > 0) {
            if(c == ')') {
                b -= 1;
                if(b == 0) ret += " ";
            }
        } else if(c == '(') {
            b += 1;
        } else {
            ret += c;
        }
    }
    console.log(ret);
    return ret;
}

function extractProductName(title) {
    setStatus("Extracting title...");
    var sep = "|:-";
    productTitle = title;
    if(title.indexOf("Amazon.in") != -1) {
        productTitle = title.substring(0, title.indexOf(":"));
    } else if(title.indexOf("Flipkart.com") != -1) {
        productTitle = title.substring(0, title.indexOf("-"));
    } else {
        var till = title.length;
        for(var i = 0; i < sep.length; ++i) {
            var idx = title.indexOf(sep[i]);
            if(idx != -1 && idx < till) till = idx;
        }
        productTitle = productTitle.substring(0, till);
    }
    productTitle = removeBrackets(productTitle);
    productTitle = productTitle.trim();
    console.log(productTitle);

    return productTitle;
}

function populateDisplay(i, p) {
    imageURL = p.imageURL;
    productURL = p.productURL;
    price = p.price / 100;
    name = p.name;
    logoURL = p.site + ".png";

    $("#image" + i).attr("src", imageURL);
    $("#price" + i).html("&#8377; " + price);
    $("#name" + i).html(name);
    $("#logo" + i).attr("src", logoURL);
    $("#link" + i).attr("href", productURL);
}

function parseResult(result) {
    console.log(result);
    var html = "";
    for(var i = 0; i < result.length; i += 3) {
        html += getHTMLForRow(result.slice(i, i + 3));
    }
    $("#results").append(html);
    nextFetchDistance = $(".row").height();
    setStatus("Enjoy!");
}

function getHTMLForRow(items) {
    html = "<div class='row'>\n";
    for(var i = 0; i < 3; i++) {
        var cur = items[i % items.length];
        html += getHTMLFor(cur);
    }
    html += "</div>\n";
    return html;
}

function getHTMLFor(item) {
    var html = '<div class="col-sm-4 col-md-4">\n';
    html += '   <a target="_blank" href="' + item.productURL + '">\n';
    html += '   <div class="preview">\n';
    html += '       <div class="image">\n';
    html += '           <img src="' + item.imageURL + '" alt="" class="thumbnail"/>\n';
    html += '       </div>\n';
    html += '       <div class="caption">\n';
    html += '           <h5><h5 class="productName">' + item.name + '</h5></h5>\n';
    html += '       </div>\n';
    html += '       <div class="row">\n';
    html += '            <div class="col-md-6 pull-left">\n';
    html += '               <h4> &#8377; ' + (item.price / 100) + '.' + (item.price % 100) + '</h4>\n';
    html += '           </div>\n';
    html += '           <div class="col-md-3 pull-right">\n';
    html += '               <img src="' + item.site + '.png" width="30"/>\n';
    html += '           </div>\n';
    html += '       </div>\n';
    html += '   </div>\n';
    html += '   </a>\n';
    html += '</div>\n';
    return html;
}

function search(offset) {
    setStatus("Querying server for " + searchText + "...");
    var queryURL = "http:localhost:8080/search?query=" + encodeURI(searchText) + "&offset=" + offset;

    var xhr = new XMLHttpRequest();
    xhr.open("GET", queryURL, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onloadend = function() {
        var result = JSON.parse(xhr.responseText);
        $.merge(history, result);
        parseResult(result);
    };
    rawData = {};
    data = JSON.stringify(rawData);

    console.log(data);
    xhr.onerror = function() {
        setStatus("Sorry. Some error occured while querying server");
    }
    xhr.send(data);
}

function watchScrollPosition(callback, interval) {
    var $window = $(window),
        $document = $(document);

    var checkScrollPosition = function() {
        var top = $document.height() - $window.height() - nextFetchDistance;

        if ($window.scrollTop() >= top) {
            callback();
        }
    };

    setInterval(checkScrollPosition, interval);
}

function getActiveTab() {
    var queryFilter = {
        active: true,
        currentWindow: true
    };
    chrome.tabs.query(queryFilter, function(tabs) {
        searchText = extractProductName(tabs[0].title);
        setup()
            });
}

function setup() {
    search(0);
    watchScrollPosition(
        function() {
            search(history.length);
        },
        2050);
}

function retreiveSearch(items) {
    if(items.search == null) {
        getActiveTab();
    } else {
        searchText = items.search;
        chrome.storage.local.remove("search");
        setup();
    }
}

function start() {
    var term = chrome.storage.local.get("search", retreiveSearch);
}

document.addEventListener('DOMContentLoaded', start);
